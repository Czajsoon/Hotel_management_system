import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {COMPONENTS, ROOT_MODULES} from "./index";
import {CookieService} from "ngx-cookie-service";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {HMSInterceptor} from "./shared/interceptors/hms-interceptor.service";
import {TwoFactorAuthenticationComponent} from './two-factor-authentication/two-factor-authentication.component';
import {TwoFactorStepComponent} from './login/two-factor-step/two-factor-step.component';


@NgModule({
  declarations: [
    ...COMPONENTS,
    TwoFactorAuthenticationComponent,
    TwoFactorStepComponent,
  ],
  imports: [
    ...ROOT_MODULES
  ],
  providers: [CookieService, {
    provide: HTTP_INTERCEPTORS,
    useClass: HMSInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
