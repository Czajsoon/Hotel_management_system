import {AppRoutingModule} from "./app-routing.module";
import {HttpClientModule} from "@angular/common/http";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {BrowserModule} from "@angular/platform-browser";
import {AppComponent} from "./app.component";
import {LoginComponent} from "./login/login.component";
import {NavbarComponent} from "./navbar/navbar.component";
import {NavbarItemComponent} from "./navbar/navabr-item/navbar-item.component";
import {PageNotFoundComponent} from "./page-not-found/page-not-found.component";
import {HotelRoomsModule} from "./hotel-rooms/hotel-rooms.module";
import {SharedModule} from "./shared/shared.module";
import {ManageAccountsModule} from "./manage-accounts/manage-accounts.module";


export const ROOT_MODULES = [
  BrowserModule,
  AppRoutingModule,
  HttpClientModule,
  BrowserAnimationsModule,
  HotelRoomsModule,
  ManageAccountsModule,
  SharedModule
]

export const COMPONENTS = [
  AppComponent,
  LoginComponent,
  NavbarComponent,
  NavbarItemComponent,
  PageNotFoundComponent
]


