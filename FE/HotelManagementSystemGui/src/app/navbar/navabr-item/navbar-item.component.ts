import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'hms-navbar-item',
  templateUrl: './navbar-item.component.html',
  styleUrls: ['./navbar-item.component.scss']
})
export class NavbarItemComponent implements OnInit {

  @Input("text") text: string;
  @Input("url") url: string;
  @Input("icon") icon: string;

  constructor(private router: Router) {
  }

  ngOnInit(): void {
  }

  public navigate(): void {
    this.router.navigate([this.url])
  }

  public isRoutingActive(): boolean {
    return this.router.isActive(this.url, true);
  }
}
