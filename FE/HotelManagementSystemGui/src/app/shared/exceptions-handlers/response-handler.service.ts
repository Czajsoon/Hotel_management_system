import {Injectable} from '@angular/core';
import {MessageService} from "primeng/api";
import {HttpErrorResponse} from "@angular/common/http";
import {tap} from "rxjs";
import {ErrorMessage, ErrorTitle, SuccessMessage, SuccessTitle} from "./messages";

@Injectable({
  providedIn: 'root'
})
export class ResponseHandlerService {

  constructor(private messageService: MessageService) {
  }

  handle(
    successMessage: SuccessMessage = SuccessMessage.SUCCESS,
    successTitle: SuccessTitle = SuccessTitle.SUCCESS,
    errorMessage: ErrorMessage = ErrorMessage.ERROR,
    errorTitle: ErrorTitle = ErrorTitle.ERROR) {
    return response$ => response$.pipe(
      this.handleSuccess(successMessage, successTitle),
      this.handleError(errorMessage, errorTitle)
    )
  }

  public handleSuccess(successMessage: SuccessMessage = SuccessMessage.SUCCESS,
                       successTitle: SuccessTitle = SuccessTitle.SUCCESS) {
    return response$ => response$.pipe(
      tap({
        complete: () => {
          this.messageService.add({
            severity: 'success',
            summary: successTitle,
            detail: successMessage
          })
        }
      })
    )
  }

  public handleError(errorMessage: ErrorMessage = ErrorMessage.ERROR,
                     errorTitle: ErrorTitle = ErrorTitle.ERROR) {
    return response$ => response$.pipe(
      tap({
        error: (e: HttpErrorResponse) => {
          this.messageService.add({severity: 'error', summary: errorTitle, detail: errorMessage + e.error})
        }
      })
    )
  }

}
