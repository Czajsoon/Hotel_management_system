import {Pipe, PipeTransform} from '@angular/core';
import {Role} from "../../models/account";

@Pipe({
  name: 'enumRole'
})
export class EnumRoleResolverPipe implements PipeTransform {

  transform(value: Role): string {
    switch (value) {
      case Role.ADMIN:
        return "Administrator";
      case Role.EMPLOYEE:
        return "Pracownik";
      default:
        return "";
    }
  }

}
