import {NgModule} from '@angular/core';
import {COMPONENTS} from "./index";
import {SharedModule} from "../shared/shared.module";
import {CommonModule} from "@angular/common";
import {AddAccountFormComponent} from './add-account/add-account-form/add-account-form.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import {MatInputModule} from "@angular/material/input";


@NgModule({
  declarations: [
    ...COMPONENTS,
    AddAccountFormComponent,
    ChangePasswordComponent
  ],
    imports: [
        CommonModule,
        SharedModule,
        MatInputModule
    ]
})
export class ManageAccountsModule {
}
