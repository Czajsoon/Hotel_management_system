import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {AccountDetails, RoleName} from "../../models/account";

@Component({
  selector: 'hms-account-search-form',
  templateUrl: './account-search-form.component.html',
  styleUrls: ['./account-search-form.component.scss']
})
export class AccountSearchFormComponent implements OnInit {
  @Output() searchParams: EventEmitter<AccountDetails> = new EventEmitter<AccountDetails>();
  @Input() availableRoles: RoleName[] = [];
  form: FormGroup;

  constructor(private fb: FormBuilder) {
    this.buildForm();
  }

  ngOnInit(): void {
  }

  buildForm(): void {
    this.form = this.fb.group({
      id: null,
      username: null,
      firstName: null,
      secondName: null,
      role: null
    })
  }

  search() {
    this.searchParams.emit(this.form.value)
  }

  clearFilters() {
    this.form.reset();
  }
}
