import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {ResponseHandlerService} from "../shared/exceptions-handlers/response-handler.service";
import {Observable} from "rxjs";
import {AccountDetail, AccountDetailsPage, AccountPasswordChange, AccountRegister} from "../models/account";
import {NavigationService} from "../shared/navigate/navigation.service";
import routing from "../../environments/routing";
import {TokenHandlerService} from "../shared/token-handler/token-handler.service";
import {ChangeTwoFactor} from "../models/TwoFactor";

@Injectable({
  providedIn: 'root'
})
export class AccountRestService {

  constructor(private http: HttpClient,
              private handler: ResponseHandlerService,
              private navigation: NavigationService) {
  }

  public getAccountDetail(): Observable<AccountDetail> {
    return this.http.get<AccountDetail>("/services-rest/account/details").pipe(
      this.handler.handleError()
    )
  }

  public getAccounts(params: any, page: number): Observable<AccountDetailsPage> {
    return this.http.get<AccountDetailsPage>("/services-rest/account/admin/details/all", {
      params: AccountRestService.mapFindAccountParams(params, page)
    }).pipe(
      this.handler.handleError()
    )
  }

  public registerAccount(body: AccountRegister) {
    return this.http.post("/services-rest/authenticate/admin/register", body).pipe(
      this.handler.handle()
    )
  }

  public change2FA(params: ChangeTwoFactor) {
    return this.http.put(`/services-rest/authenticate/admin/change-two-factor/${params.accountId}/${params.value}`, null).pipe(
      this.handler.handle()
    )
  }

  public reset2FA(accountId: string) {
    return this.http.put(`/services-rest/authenticate/admin/reset-two-factor/${accountId}`, null).pipe(
      this.handler.handle()
    )
  }

  public deleteAccount(accountId: string) {
    return this.http.delete(`/services-rest/account/admin/delete/${accountId}`).pipe(
      this.handler.handle()
    )
  }

  private static mapFindAccountParams(params: any, page: number) {
    let httpParams = new HttpParams();
    if (params) {
      httpParams = params.id ? httpParams.append("id", params.id) : httpParams;
      httpParams = params.username ? httpParams.append("username", params.username) : httpParams;
      httpParams = params.firstName ? httpParams.append("firstName", params.firstName) : httpParams;
      httpParams = params.secondName ? httpParams.append("secondName", params.secondName) : httpParams;
      httpParams = params.role ? httpParams.append("role", params.role) : httpParams;
      httpParams = page ? httpParams.append("page", page) : httpParams;
      return httpParams;
    } else return httpParams;
  }

  public getQrCode() {
    return this.http.get("/services-rest/authenticate/qrcode", {responseType: 'blob'}).pipe(this.handler.handle());
  }

  public validateTwoFaCode(code: string, tokenHandlerService: TokenHandlerService) {
    return this.http.put(`/services-rest/authenticate/validate/${code}`, null).pipe(
      this.handler.handleError(),
      tokenHandlerService.setTwoFaValidatedOnSuccess(),
      this.navigation.navigate(routing.rooms)
    )
  }

  public changePassword(accountPasswordChange: AccountPasswordChange){
    return this.http.post("/services-rest/authenticate/changePassword",accountPasswordChange).pipe(this.handler.handle());
  }

}
