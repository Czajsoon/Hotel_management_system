import {NgModule} from '@angular/core';
import {SharedModule} from "../shared/shared.module";
import {COMPONENTS} from "./index";
import {MatGridListModule} from "@angular/material/grid-list";

@NgModule({
  declarations: [
    ...COMPONENTS
  ],
  imports: [
    SharedModule, MatGridListModule
  ]
})
export class HotelRoomsModule {
}
