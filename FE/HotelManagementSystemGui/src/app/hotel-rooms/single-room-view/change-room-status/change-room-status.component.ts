import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {RoomStatus, RoomStatusNames, roomStatusNamesValues} from "../../../models/room";
import {RoomStatusPipe} from "../../../shared/pipes/room-status.pipe";
import {RoomRestService} from "../../../rest-services/room-rest.service";

@Component({
  selector: 'hms-change-room-status',
  templateUrl: './change-room-status.component.html',
  styleUrls: ['./change-room-status.component.scss']
})
export class ChangeRoomStatusComponent implements OnInit {
  displayModal: boolean = false;
  form: FormGroup;
  roomStatusPipe: RoomStatusPipe = new RoomStatusPipe();
  roomStatuses: RoomStatusNames[] = roomStatusNamesValues(this.roomStatusPipe);
  @Input() roomId: string;
  @Input() roomStatus: RoomStatus;

  constructor(
    private fb: FormBuilder,
    private roomRest: RoomRestService
  ) {
    this.buildForm()
  }

  ngOnInit(): void {
    this.form.setValue({status: this.roomStatus});
  }

  changeVisibleModal() {
    this.displayModal = !this.displayModal;
  }

  buildForm(): void {
    this.form = this.fb.group({
      status: [this.roomStatus, Validators.required],
    })
  }

  change() {
    const formValue = this.form.value;
    this.roomRest.updateRoomStatus(this.roomId, formValue.status).subscribe()
  }

  validChange(): boolean {
    return this.form.invalid || this.roomStatus === this.form.get('status').value
  }
}
