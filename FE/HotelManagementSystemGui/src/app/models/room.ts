import {RoomStatusPipe} from "../shared/pipes/room-status.pipe";

export interface RoomDetails {
  id: string,
  level: number;
  roomStatus: RoomStatus;
  pricePerNight: number;
  balconyAvailable: boolean;
  bedsNumber: number;
  //description: string,
  //comments: string
}

export enum RoomStatus {
  DISABLED = "DISABLED",
  AVAILABLE = "AVAILABLE",
  TO_PREPARE = "TO_PREPARE",
  OCCUPIED = "OCCUPIED"
}

export interface RoomStatusNames {
  name: string;
  value: RoomStatus;
}


export function RoomStatusOf(value: string): RoomStatus {
  switch (value) {
    case "DISABLED":
      return RoomStatus.DISABLED
    case "AVAILABLE":
      return RoomStatus.AVAILABLE
    case "TO_PREPARE":
      return RoomStatus.TO_PREPARE
    case "OCCUPIED":
      return RoomStatus.OCCUPIED
    default:
      return null
  }
}

export function roomStatusNamesValues(roomStatusPipe: RoomStatusPipe): RoomStatusNames[] {
  return Object.values(RoomStatus).map(status => {
    let roomStatus = RoomStatusOf(status);
    return {name: roomStatusPipe.transform(roomStatus), value: roomStatus};
  })
}

export interface RoomStatusName {
  name: string;
  value: RoomStatus;
}

export interface RoomDetailsPage {
  totalElements: number;
  totalPages: number;
  rooms: RoomDetails[];
}

export interface BalconyAvailable{
  label:string,
  value:boolean
}

export interface AddRoomDTO{
  level:number,
  pricePerNigh:number,
  balconyAvailable: boolean,
  bedsNumber: number
}
