# Hotel Management System

HMS to aplikacja webowa, która prezentuje system zarządzania hotelem. Wspomaga ona pracowników oraz właściciela hotelu w
zarządzaniu pokojami hotelowymi.

## Spis treści

- [Wymagane oprogramowanie](#Wymagane oprogramowanie)
- [Instalacja](#Instalacja)
- [Baza danych](#Baza Danych)
- [Funkcjonalności i ich realizacja](#Funkcjonalności i ich realizacja)
- [Autorzy](#Autorzy)

## Opis projektu

Krótki opis projektu.

## Wymagane oprogramowanie

- JDK w wersji 17
- Node.js
- Angular CLI (Command Line Interface)
- Intellij Idea
- WebStorm lub Visual Studio Code

### Instalacja

Jak zainstalować i uruchomić projekt.

1. Pobranie projektu z GitLab

- `git@gitlab.com:Czajsoon/Hotel_management_system.git`
- `https://gitlab.com/Czajsoon/Hotel_management_system.git`

2. Start projektu angular
   - należy przejść do folderu "HotelManagementSystemGui" i otworzyć konsolę następnie wpisać
     polecenie: <br> `npm install`
   - wystartować projekt Angular poprzez polecenie: <br> `ng serve`
     <br> aplikacja automatycznie będzie hostowana lokalnie na porcie 4200 <br>
     link url: `http://localhost:4200/`

## Funkcjonalności i ich realizacja

### Zadania wykonane przez Jakub Czajkowski

#### Backend

- StructMapper z lombok <span style="color:green">DONE</span>
- SonarQube <span style="color:green">DONE</span>
- Jacoco <span style="color:green">DONE</span>
- SpringSecurity <span style="color:green">DONE</span>
- H2 dla testów <span style="color:green">DONE</span>

Wystawione resty

- `/services-rest/account/details` <span style="color:green">DONE</span>
- `/services-rest/account/admin/details/all` <span style="color:green">DONE</span>
- `/services-rest/account/admin/delete/{accountId}` <span style="color:green">DONE</span>
- `/services-rest/authenticate/login` <span style="color:green">DONE</span>
- `/services-rest/authenticate/admin/register` <span style="color:green">DONE</span>

#### Frontend

- PrimeNG <span style="color:green">DONE</span>
- Angular Materials <span style="color:green">DONE</span>
- Bootstrap <span style="color:green">DONE</span>
- Obsługa tokenów <span style="color:green">DONE</span>
- Obsługa ładowania rootingów oraz zapytań HTTP <span style="color:green">DONE</span>
- Stworzenie oraz obsługa strony 404 <span style="color:green">DONE</span>
- Stworzenie oraz obsługa strony logowania <span style="color:green">DONE</span>
- Stworzenie obsługi dla zapytań HTTP <span style="color:green">DONE</span>
- Stworzenie widoku wszystkich użytkowników dla aministratora <span style="color:green">DONE</span>
- Stworzenie nawigacji aplikacji <span style="color:green">DONE</span>

### Wszyscy użytkownicy:

#### Logowanie, 2FA, tokeny

- (bartek 2fa <span style="color:green">DONE</span>)
- (kuba tokeny <span style="color:green">DONE</span>)

#### Zmiana hasła, walidacja (bartek) <span style="color:green">DONE</span>

### Administator:

#### Tworzenie kont pracowników (kuba)

#### Dodawanie pokoi, (bartek) <span style="color:green">DONE</span>

#### Zmiana statusów pokoi (kuba)

#### Wyświetlanie pokoi (bartek) <span style="color:green">DONE</span>

### Pracownik hotelu

- Statusy pokoi: DISABLED,AVAILABLE,TO_PREPARE,OCCUPIED
- Role użytkowników: ADMIN,EMPLOYEE

Strona startowa, tabela z pokojami

## Autorzy

Autorzy projektu:

1. Jakub Czajkowski (Gitlab: `https://gitlab.com/Czajsoon`)
2. Bartosz Kuta (Gitlab: `https://gitlab.com/bkprog`)
